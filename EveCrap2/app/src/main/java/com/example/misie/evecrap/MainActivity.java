package com.example.misie.evecrap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private User loggedUser;
    UserLocalStore userLocalStore;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference();

    private Map<String, String> admins = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        userLocalStore = new UserLocalStore(this);

//        Event sth = new Event();
//        sth.setName("Event 1.5");
//        sth.setDescription("Description 4");
//        sth.setDate(new Date());
//
//        String key = myRef.child("Events").push().getKey();
//        sth.setId(key);
//        myRef.child("Events").child(key).setValue(sth);

        myRef.addValueEventListener(listener);
    }


    @Override
    protected void onStart() {
        super.onStart();

        if(!userLocalStore.getUserLoggedIn())
            loginWindow();
        else{
            TextView helloView = (TextView) findViewById(R.id.hello);
            loggedUser = userLocalStore.getStoredUser();
            helloView.setText("Zalogowano: " + loggedUser.getUsername() + " " + loggedUser.getEmail());

            //myRef.child("Users").child(loggedUser.getId()).setValue(loggedUser);

        }
    }

    private boolean setUserPrivigs(User loggedUser) {
        if(loggedUser == null) return false;
        for( Map.Entry<String, String> n : admins.entrySet()){
            if(n.getKey().equals(loggedUser.getId())){
                if(n.getValue().equals(loggedUser.getEmail())){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAuth = FirebaseAuth.getInstance();
        if(userLocalStore.getUserLoggedIn()) loggedUser = userLocalStore.getStoredUser();

    }

    public void logOut(View view){
        signOut();
    }

    private void signOut(){
        mAuth.signOut();
        loggedUser = null;
        userLocalStore.clearUserData();
        userLocalStore.setUserLoggedIn(false);

        TextView helloView = (TextView) findViewById(R.id.hello);
        helloView.setText("Wylogowano");

        loginWindow();
    }

    private void loginWindow(){
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public void createEventWindow(View view){
        Intent intent = new Intent(this, createEventActivity.class);
        startActivity(intent);
    }

    ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for(DataSnapshot ds : dataSnapshot.getChildren()) {
                if(ds.getKey().toString().equals("Events"))
                    getEvents(ds);
                else if(ds.getKey().toString().equals("Admins"))
                    getAdmins(ds);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private void getAdmins(DataSnapshot dataSnapshot) {
        admins = (Map<String, String>) dataSnapshot.getValue();

        userLocalStore.setUserPrivigs(setUserPrivigs(loggedUser));
        if(userLocalStore.getUserPrivigs())
            findViewById(R.id.createButton).setVisibility(View.VISIBLE);
    }


    public void getEvents(DataSnapshot dataSnapshot){
        ArrayList<Event> events = new ArrayList<Event>();

        for (DataSnapshot dss : dataSnapshot.getChildren() ) {
            //for(DataSnapshot dss : ds.getChildren()) {

                //String key = dss.getValue().toString();
                Event tmp = dss.getValue(Event.class);

                //Event tmp = ds.getValue(Event.class);
                events.add(tmp);
            //}
        }

        final ListView eventsLView = findViewById(R.id.eventsListView);

        EventsAdapter adapter = new EventsAdapter(this, events);

        eventsLView.setAdapter(adapter);
        eventsLView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Event e = (Event) eventsLView.getItemAtPosition(position);

                Toast.makeText(getBaseContext(), e.getName(), Toast.LENGTH_SHORT).show();
                viewEventInfo(e);
            }
        });
    }

    private void viewEventInfo(Event event){
        Intent intent = new Intent(this, EventActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
    }

}
