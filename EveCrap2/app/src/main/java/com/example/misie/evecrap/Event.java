package com.example.misie.evecrap;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by misie on 16.11.2017.
 */

public class Event implements Serializable{

    //private static final AtomicInteger count = new AtomicInteger(0);
    private String Id;
    private String Name;
    private String Description;
    private Date Date;
    private Map<String, String> Participants = new HashMap<>();

    public Event() {
    }

    public Event(String id, String name, String description, Date date, Map<String, String> participants) {
        Id = id;
        Name = name;
        Description = description;
        Date = date;
        Participants = participants;
    }


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Date getDate() {
        return Date;
    }

    public void setDate(Date date) {
        Date = date;
    }

    public Map<String, String> getParticipants() {
        return Participants;
    }

    public void setParticipants(Map<String, String> participants) {
        Participants = participants;
    }
}
