package com.example.misie.evecrap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.Date;

public class CalendarActivity extends AppCompatActivity {

    private DatePicker datePicker;
    private Button saveDate;

    private Event event;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        event = (Event) getIntent().getSerializableExtra("event");

        datePicker = findViewById(R.id.datePicker);
        saveDate = findViewById(R.id.saveDateButton);
    }

    private void chooseDate(){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth() +1;
        int year = datePicker.getYear();

        event.setDate(new Date(year-1900, month-1, day));
    }



    public void SaveDate(View view){
        chooseDate();
        Intent intent = new Intent(this, createEventActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
        this.finish();
    }
}
