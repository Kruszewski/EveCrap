package com.example.misie.evecrap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by misie on 15.11.2017.
 */

public class User implements Serializable {

    private String Id;
    private String Username;
    private String Email;
    private Map<String, String> myEvents = new HashMap<>();


    public User() {
    }

    public User(String id, String username, String email, Map<String, String> myEvents) {

        Id = id;
        Username = username;
        Email = email;
        this.myEvents = myEvents;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public Map<String, String> getMyEvents() {
        return myEvents;
    }

    public void setMyEvents(Map<String, String> myEvents) {
        this.myEvents = myEvents;
    }
}
