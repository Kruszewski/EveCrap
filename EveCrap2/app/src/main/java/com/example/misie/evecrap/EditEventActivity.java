package com.example.misie.evecrap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditEventActivity extends AppCompatActivity {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference();

    private Event event;
    private EditText editTitle;
    private EditText editDesc;
    private TextView editDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);

        Intent intent = getIntent();
        event = (Event) intent.getSerializableExtra("event");

        editTitle = findViewById(R.id.editTitle);
        editDesc = findViewById(R.id.editDesc);
        editDate = findViewById(R.id.editDate);

        editTitle.setText(event.getName());
        editDesc.setText(event.getDescription());
        int s = event.getDate().getDay();
        int s1 = event.getDate().getMonth();
        int s2 = event.getDate().getYear();
        editDate.setText(""+ event.getDate().getDate() + "." + (event.getDate().getMonth()+1) + "." + (event.getDate().getYear()+1900));
    }


    public void Save(View view){
        saveChanges();
        Toast.makeText(this, "Changes saved", Toast.LENGTH_SHORT);
        backToEventInfo();
        this.finish();
    }

    public void Cancel(View view){
        backToEventInfo();
        this.finish();
    }


    public void Calendar(View view){
        setEventValues();
        Intent intent = new Intent(this, editCalendarActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
        this.finish();
    }

    private void setEventValues() {
        event.setName(editTitle.getText().toString());
        event.setDescription(editDesc.getText().toString());
    }


    private void saveChanges(){
        setEventValues();
        myRef.child("Events").child(event.getId()).setValue(event);
    }

    private void backToEventInfo(){
        Intent intent = new Intent(this, EventActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
    }
}
