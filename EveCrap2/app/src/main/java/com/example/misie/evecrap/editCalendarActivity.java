package com.example.misie.evecrap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

public class editCalendarActivity extends AppCompatActivity {

    private DatePicker datePicker;
    private Button saveDate;

    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_calendar);



        event = (Event) getIntent().getSerializableExtra("event");

        Date date = event.getDate();

        datePicker = findViewById(R.id.datePicker);
        datePicker.updateDate(date.getYear()+1900, date.getMonth(), date.getDate());
        saveDate = findViewById(R.id.saveDateButton);
    }

    private void chooseDate(){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth() +1;
        int year = datePicker.getYear();


        Date tmp = new Date(year-1900, month-1, day);

        int th = tmp.getDate();
        int th2 = tmp.getMonth()+1;
        int th3 = tmp.getYear()+1900;

        event.setDate(new Date(year-1900, month-1, day));
    }



    public void SaveDate(View view){
        chooseDate();
        Intent intent = new Intent(this, EditEventActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
        this.finish();
    }

}
