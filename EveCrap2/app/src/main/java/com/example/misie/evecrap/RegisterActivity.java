package com.example.misie.evecrap;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    UserLocalStore userLocalStore;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        userLocalStore = new UserLocalStore(this);
        if(userLocalStore.getUserLoggedIn()) MainWindow();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        userLocalStore = new UserLocalStore(this);
    }



    public void register(View view){
        EditText editemail = (EditText)findViewById(R.id.emailRegister);
        String email = editemail.getText().toString();

        EditText editPassword = (EditText) findViewById(R.id.pwdRegister);
        String pwd = editPassword.getText().toString();

        EditText editPassword2 = (EditText) findViewById(R.id.secPwdRegister);
        String pwdSec = editPassword2.getText().toString();

        createAccount(email, pwd, pwdSec);
        //this.finish();
    }


    private void createAccount(String email, String pwd, String secPwd){
        Log.d("register", "createAccount: " + email);
        if(email != null && pwd != null && pwd.equals(secPwd)){
            mAuth.createUserWithEmailAndPassword(email,pwd)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Log.d("register", "createUsenWithEmail: success");
                                setLoggedUser();
                                exportUserToDatabase();

                                MainWindow();
                            }
                            else{
                                Log.d("register", "createUsenWithEmail: " + task.getException());
                            }
                        }
                    });
        }

    }

    private void MainWindow() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void exportUserToDatabase() {
        FirebaseUser authUser = mAuth.getCurrentUser();

        User user = new User();
        user.setId(authUser.getUid());
        user.setEmail(authUser.getEmail());
        user.setUsername((authUser.getDisplayName()!=null) ? authUser.getDisplayName() : "brak");

        myRef.child("Users").child(user.getId()).setValue(user);
    }


    public void setLoggedUser(){
        FirebaseUser user = mAuth.getCurrentUser();

        String id = user.getUid();
        String username =  (user.getDisplayName()!=null) ? user.getDisplayName() : "brak";
        String mail = user.getEmail();

        User sth = new User(id, username, mail, null);
        userLocalStore.storeUserData(sth);
        userLocalStore.setUserLoggedIn(true);
    }
}
