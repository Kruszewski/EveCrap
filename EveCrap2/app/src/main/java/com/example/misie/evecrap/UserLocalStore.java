package com.example.misie.evecrap;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by misie on 16.11.2017.
 */

public class UserLocalStore {

    public static final String SP_NAME = "userDetails";
    SharedPreferences userLocal;


    public UserLocalStore(Context context) {
        userLocal = context.getSharedPreferences(SP_NAME, 0);
    }

    public void storeUserData(User user){
        SharedPreferences.Editor spEditor = userLocal.edit();
        spEditor.putString("Id", user.getId());
        spEditor.putString("Username", user.getUsername());
        spEditor.putString("Mail", user.getEmail());

        spEditor.commit();
    }

    public void setUserPrivigs(Boolean admin){
        SharedPreferences.Editor spEditor = userLocal.edit();
        spEditor.putBoolean("Admin", admin);
        spEditor.commit();
    }

    public Boolean getUserPrivigs(){
        return userLocal.getBoolean("Admin", false);
    }

    public User getStoredUser(){
        String id = userLocal.getString("Id", "");
        String username = userLocal.getString("Username", "");
        String mail = userLocal.getString("Mail", "");

        User storedUser = new User(id, username, mail, null);
        return storedUser;
    }

    public void setUserLoggedIn(boolean loggedIn){
        SharedPreferences.Editor spEditor = userLocal.edit();
        spEditor.putBoolean("loggedIn", loggedIn);
        spEditor.commit();
    }

    public boolean getUserLoggedIn(){
        if(userLocal.getBoolean("loggedIn", false)){
            return true;
        }
        else return false;
    }

    public void clearUserData(){
        SharedPreferences.Editor spEditor = userLocal.edit();
        spEditor.clear();
    }
}
