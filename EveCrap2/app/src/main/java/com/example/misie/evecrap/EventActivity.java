package com.example.misie.evecrap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventActivity extends AppCompatActivity {

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference();

    private UserLocalStore userLocalStore;
    private User loggedUser;
    private User databaseUser;
    private ArrayList<User> databaseUsers = new ArrayList<User>();

    private Switch join;

    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        userLocalStore = new UserLocalStore(this);
        loggedUser = userLocalStore.getStoredUser();
        myRef.addValueEventListener(listener);

        Intent intent = getIntent();
        event = (Event)intent.getSerializableExtra("event");

        join = findViewById(R.id.joinSwitch);

        setView(event);
        join.setOnCheckedChangeListener(switchListener);
    }

    ValueEventListener listener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            databaseUser = dataSnapshot.child("Users").child(loggedUser.getId()).getValue(User.class);
            for(DataSnapshot ds : dataSnapshot.getChildren()){
                if(ds.getKey().equals("Users")){
                    for(DataSnapshot dss : ds.getChildren()) {
                        User tmp = dss.getValue(User.class);
                        databaseUsers.add(tmp);
                    }
                }

            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

     CompoundButton.OnCheckedChangeListener switchListener =  new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(alreadyJoined()){
                leaveEvent();
            }
            else if(!alreadyJoined()){
                joinEvent();
            }

            refreshWindow();
        }
    };

    private void setView(Event event){
        TextView name = (TextView) findViewById(R.id.evName);
        TextView desc = (TextView) findViewById(R.id.evDesc);
        TextView date = findViewById(R.id.evDate);
//        if(alreadyJoined()) {
//            findViewById(R.id.joinButton).setVisibility(View.GONE);
//            findViewById(R.id.leaveButton).setVisibility(View.VISIBLE);
//        }
        join.setChecked(alreadyJoined());

        Date tmp  = event.getDate();

        name.setText(event.getName());
        desc.setText(event.getDescription());
        date.setText(""+ tmp.getDate()+"."+(tmp.getMonth()+1)+"."+(tmp.getYear()+1900));

        if(event.getParticipants() != null)
            createParticipantsList(event.getParticipants());

        if(userLocalStore.getUserPrivigs())
        {
            findViewById(R.id.editButton).setVisibility(View.VISIBLE);
            findViewById(R.id.deleteButton).setVisibility(View.VISIBLE);
        }

//        ListView participantsLView = (ListView) findViewById(R.id.evParticipants);
//        participantsLView.setVisibility(View.GONE);

        //UsersAdapter adapter = new UsersAdapter(this, event.getParticipants());

    }

    private void createParticipantsList(Map<String, String> users) {
        ArrayList<String> emails = new ArrayList<String>();
        for(Map.Entry<String, String> us : users.entrySet()){
            emails.add(us.getValue());
        }
        ListView participants = (ListView) findViewById(R.id.evParticipants);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, emails);
        participants.setAdapter(adapter);

    }

    public void Join(View view){
        if(!alreadyJoined()) {
            joinEvent();
            refreshWindow();
        }
    }

    private void joinEvent(){

        Map<String, String> participants = event.getParticipants();
        participants.put(loggedUser.getId(), loggedUser.getEmail());
        event.setParticipants(participants);



        Map<String, String> userEvents = databaseUser.getMyEvents(); //loggedUser.getMyEvents();

        if(userEvents == null) userEvents = new HashMap<>();

        userEvents.put(event.getId(), event.getName());
        loggedUser.setMyEvents(userEvents);

        myRef.child("Events").child(event.getId()).setValue(event);
        myRef.child("Users").child(loggedUser.getId()).setValue(loggedUser);
    }

    private boolean alreadyJoined(){

        Map<String, String> eventParticipants = event.getParticipants();
        for(Map.Entry<String, String> n : eventParticipants.entrySet()){
            if(n.getKey().equals(loggedUser.getId()) && n.getValue().equals(loggedUser.getEmail()))
                return true;
        }
        return false;
    }

    public void Leave(View view){
        if(alreadyJoined()) {
            leaveEvent();
            refreshWindow();
        }
    }

    private void leaveEvent() {

        Map<String, String> participants = event.getParticipants();
        participants.remove(loggedUser.getId());
        event.setParticipants(participants);

        Map<String, String> userEvents = databaseUser.getMyEvents();
        userEvents.remove(event.getId());
        loggedUser.setMyEvents(userEvents);

        myRef.child("Events").child(event.getId()).setValue(event);
        myRef.child("Users").child(loggedUser.getId()).setValue(loggedUser);
    }


    public void Edit(View view){
        Intent intent = new Intent(this, EditEventActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
        this.finish();
    }

    public void Delete(View view){
        if(userLocalStore.getUserPrivigs()){
            deleteEvent();

            startActivity(new Intent(this, MainActivity.class));
            this.finish();
        }
    }

    private void deleteEvent(){
        for(User n : databaseUsers){
            Map<String, String> events = n.getMyEvents();
            events.remove(event.getId());
            n.setMyEvents(events);
            myRef.child("Users").child(n.getId()).setValue(n);
        }
        myRef.child("Events").child(event.getId()).removeValue();
    }

    private void refreshWindow(){
        this.finish();
        Intent intent = new Intent(this, EventActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
    }

}
