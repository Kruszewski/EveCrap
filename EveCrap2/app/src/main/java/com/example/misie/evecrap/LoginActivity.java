package com.example.misie.evecrap;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private User loggedUser;
    UserLocalStore userLocalStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();
        userLocalStore = new UserLocalStore(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(userLocalStore.getUserLoggedIn()) startActivity(new Intent(this, MainActivity.class));
    }

    public void signIn(View view){

        EditText editEmail = (EditText) findViewById(R.id.email);
        String email = editEmail.getText().toString();

        EditText editPwd = (EditText) findViewById(R.id.password);
        String pwd = editPwd.getText().toString();

        if(email != null && pwd != null){
            mAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        Log.d("loggingIn", "signInWithEmail: success");
                        setLoggedUser();
                    }
                    else {
                        Log.w("loggingIn", "signInWithEmail: failure", task.getException());
                        //updateUI(null);
                    }

                    if(!task.isSuccessful()){
                        //wrong data / acc doesnt exist
                    }
                }

            });
        }
        //this.finish();
    }

    public void register(View view){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }


    public void setLoggedUser(){
        FirebaseUser user = mAuth.getCurrentUser();

        String id = user.getUid();
        String username =  (user.getDisplayName()!=null) ? user.getDisplayName() : "brak";
        String mail = user.getEmail();

        User sth = new User(id, username, mail, null);
        userLocalStore.storeUserData(sth);
        userLocalStore.setUserLoggedIn(true);

        startActivity(new Intent(this, MainActivity.class));
    }

}
