package com.example.misie.evecrap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class createEventActivity extends AppCompatActivity {


    private EditText eventTitle;
    private EditText eventDescription;
    private TextView eventDate;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference();

    private Event event;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);

        eventTitle = (EditText) findViewById(R.id.eventTitle);
        eventDescription = (EditText) findViewById(R.id.eventDescription);
        eventDate = findViewById(R.id.eventDate);

        if(getIntent().getSerializableExtra("event") != null){
            event = (Event) getIntent().getSerializableExtra("event");
            setView();
        }
        else event = new Event();
    }

    private void setView() {
        eventTitle.setText(event.getName());
        eventDescription.setText(event.getDescription());
        Date date = event.getDate();
        eventDate.setText(""+ date.getDate() +"." + (date.getMonth()+1)+"." + (date.getYear()+1900));
    }


    public void Create(View view){
        createEvent();
        this.finish();
    }

    public void Cancel(View view){
        this.finish();
    }

    public void Calendar(View view){
        setEventValues();
        Intent intent = new Intent(this, CalendarActivity.class);
        intent.putExtra("event", event);
        startActivity(intent);
        this.finish();
    }

    private void createEvent() {
        setEventValues();

        String key = myRef.child("Events").push().getKey();
        event.setId(key);
        myRef.child("Events").child(key).setValue(event);

    }

    private void setEventValues(){
        event.setName(eventTitle.getText().toString());
        event.setDescription(eventDescription.getText().toString());
    }

}
